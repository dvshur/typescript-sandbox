import { compose } from 'ramda';

const identity = <A>(a: A): A => a;

class Maybe<A> {
  private constructor(private tag: 'Just' | 'Nothing', private value?: A) {}

  // inconvenient usage of factories
  public static Just<A>(value: A): Maybe<A> {
    return new Maybe<A>('Just', value);
  }

  public static Nothing<A>(): Maybe<A> {
    return new Maybe<A>('Nothing');
  }

  public static of<A>(value: A): Maybe<A> {
    return Maybe.Just(value);
  }

  public match<B>(pattern: { Just: (a: A) => B; Nothing: () => B }): B {
    switch (this.tag) {
      case 'Just':
        return pattern.Just(this.value);
      case 'Nothing':
        return pattern.Nothing();
    }
  }

  public flatMap<B>(f: (a: A) => Maybe<B>): Maybe<B> {
    return this.match({
      Just: f,
      Nothing: () => Maybe.Nothing<B>(),
    });
  }

  public getOrElse(a1: A) {
    return this.match({
      Just: identity,
      Nothing: () => a1,
    });
  }

  public map<B>(f: (a: A) => B): Maybe<B> {
    return this.flatMap(
      compose(
        // inconvenient
        (b: B) => Maybe.of(b),
        f
      )
    );
  }
}

export default Maybe;
