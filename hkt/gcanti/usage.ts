import { lift } from './functor';
import { Identity, identity } from './Identity';

import { Option, Some, None, some, none, option } from './Option';

const double = (x: number) => x * 2;

const i = new Identity(1).map(double);

const liftedDouble = lift(option)(double);

const x = option.map(x => x.toString(), some(1));

const y = x.match({
  Some: a => 2,
  None: () => 10,
});
