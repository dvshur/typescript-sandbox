import { HKT, URIS, Type } from './HKT';

export interface Functor1<F extends URIS> {
  map: <A, B>(f: (a: A) => B, fa: Type<F, A>) => Type<F, B>;
}

export interface Functor<F> {
  readonly URI: F; // container type
  map: <A, B>(f: (a: A) => B, fa: HKT<F, A>) => HKT<F, B>;
  // problem: no indication its the same functor
}

export function lift<F extends URIS>(
  F: Functor1<F>
): <A, B>(f: (a: A) => B) => (fa: Type<F, A>) => Type<F, B>;
export function lift<F>(
  F: Functor<F>
): <A, B>(f: (a: A) => B) => (fa: HKT<F, A>) => HKT<F, B> {
  return f => fa => F.map(f, fa);
}
