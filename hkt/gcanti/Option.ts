// import { HKT } from './HKT';
import { Monad1 } from './monad';
import { Functor1 } from './functor';

import { WithMap, WithChain } from './traits';

export const URI = 'Option';
export type URI = typeof URI;

// export type OptionPattern<A, B> = {
//   Some: (a: A) => B;
//   None: () => B;
// };

export abstract class Option<A> implements WithMap<A>, WithChain<A> {
  public abstract match<B>(p: { Some: (a: A) => B; None: () => B }): B;
  public abstract map<B>(f: (a: A) => B): Option<B>;
  public abstract chain<B>(f: (a: A) => Option<B>): Option<B>;

  public abstract getOrElse(a: A): A;
  public abstract orElse(opt2: Option<A>): Option<A>;
}

export class Some<A> extends Option<A> {
  constructor(private value) {
    super();
  }
  public map<B>(f: (a: A) => B): Some<B> {
    return new Some(f(this.value));
  }
  public chain<B>(f: (a: A) => Some<B>): Some<B> {
    return f(this.value);
  }
  public match<B>(p: { Some: (a: A) => B; None: () => B }): B {
    return p.Some(this.value);
  }
  public getOrElse(a: A): A {
    return this.value;
  }
  public orElse(opt2: Option<A>): Some<A> {
    return this;
  }
}

export class None<A> extends Option<A> {
  public map<B>(f: (a: A) => B): None<B> {
    return new None();
  }
  public chain<B>(f: (a: A) => Some<B>): None<B> {
    return new None();
  }
  public match<B>(p: { Some: (a: A) => B; None: () => B }): B {
    return p.None();
  }
  public getOrElse(a: A): A {
    return a;
  }
  public orElse(opt2: Option<A>): Option<A> {
    return opt2;
  }
}

declare module './HKT' {
  interface URI2HKT<A> {
    Option: Option<A>;
  }
}

export const optionFunctor: Functor1<URI> = {
  map: <A, B>(f: (a: A) => B, fa: Option<A>): Option<B> => fa.map(f),
};

export const option: Monad1<URI> = {
  ...optionFunctor,
  of: <A>(a: A): Some<A> => new Some(a),
  chain: <A, B>(f: (a: A) => Option<B>, fa: Option<A>): Option<B> =>
    fa.chain(f),
};

export const some = <A>(a: A): Option<A> => new Some<A>(a);
export const none: Option<never> = new None();
