import { HKT, URIS, Type } from './HKT';
import { Functor, Functor1 } from './functor';

export interface Monad1<F extends URIS> extends Functor1<F> {
  of: <A>(a: A) => Type<F, A>;
  chain: <A, B>(f: (a: A) => Type<F, B>, fa: Type<F, A>) => Type<F, B>;
}

export interface Monad<F> extends Functor<F> {
  readonly URI: F;
  of: <A>(a: A) => HKT<F, A>;
  chain: <A, B>(f: (a: A) => HKT<F, B>, fa: HKT<F, A>) => HKT<F, B>;
}

export { lift } from './functor';

// export function lift<F extends URIS>(
//   F: Functor1<F>
// ): <A, B>(f: (a: A) => B) => (fa: Type<F, A>) => Type<F, B>;
// export function lift<F>(
//   F: Functor<F>
// ): <A, B>(f: (a: A) => B) => (fa: HKT<F, A>) => HKT<F, B> {
//   return f => fa => F.map(f, fa);
// }
