// import { HKT } from './HKT';
import { Functor1 } from './functor';

export const URI = 'Identity';
export type URI = typeof URI;

export class Identity<A> {
  // @todo does it need to be an HKT?
  // readonly _A!: A; // --> these phantom fields make `Identity` an `HKT`, note that both `A` and `URI` here are types
  // readonly _URI!: URI; // ----^

  constructor(readonly value: A) {}

  map<B>(f: (a: A) => B): Identity<B> {
    return new Identity(f(this.value));
  }
}

const map = <A, B>(f: (a: A) => B, fa: Identity<A>): Identity<B> => {
  return fa.map(f);
};

export const identity: Functor1<URI> = {
  map,
};

declare module './HKT' {
  interface URI2HKT<A> {
    Identity: Identity<A>; // maps the key "Identity" to the type `Identity`
  }
}
