export interface WithMap<A> {
  map<B>(f: (a: A) => B): WithMap<B>;
}

export interface WithChain<A> {
  chain<B>(f: (a: A) => WithChain<B>): WithChain<B>;
}
