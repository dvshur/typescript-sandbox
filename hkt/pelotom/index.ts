namespace PelotomHKT {
  declare const index: unique symbol;

  // A type for representing type variables
  type _<N extends number = 0> = { [index]: N };

  // Type application (substitutes type variables with types)
  type $<T, S, N extends number = 0> = T extends _<N>
    ? S
    : T extends undefined | null | boolean | string | number
    ? T
    : T extends Array<infer A>
    ? $Array<A, S, N>
    : T extends (x: infer I) => infer O
    ? (x: $<I, S, N>) => $<O, S, N>
    : T extends object
    ? { [K in keyof T]: $<T[K], S, N> }
    : T;

  interface $Array<T, S, N extends number> extends Array<$<T, S, N>> {}

  // Let's declare some familiar type classes...

  interface Functor<F> {
    map: <A, B>(fa: $<F, A>, f: (a: A) => B) => $<F, B>;
  }

  interface Monad<M> {
    pure: <A>(a: A) => $<M, A>;
    bind: <A, B>(ma: $<M, A>, f: (a: A) => $<M, B>) => $<M, B>;
  }

  interface MonadLib<M> extends Monad<M>, Functor<M> {
    join: <A>(mma: $<M, $<M, A>>) => $<M, A>;
    // sequence, etc...
  }

  const Monad = <M>({ pure, bind }: Monad<M>): MonadLib<M> => ({
    pure,
    bind,
    map: (ma, f) => bind(ma, a => pure(f(a))),
    join: mma => bind(mma, ma => ma),
  });

  // ... and an instance

  type Maybe<A> = { tag: 'none' } | { tag: 'some'; value: A };
  const none: Maybe<never> = { tag: 'none' };
  const some = <A>(value: A): Maybe<A> => ({ tag: 'some', value });

  const { map, join } = Monad<Maybe<_>>({
    pure: some,
    bind: (ma, f) => (ma.tag === 'some' ? f(ma.value) : ma),
  });

  // Not sure why the `<number>` annotation is required here...
  const result = map(join<number>(some(some(42))), n => n + 1);
  // expect(result).toEqual(some(43));
}
