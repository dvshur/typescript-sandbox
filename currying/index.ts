function curry2<A, B, C>(f: (a: A, b: B) => C) {
  return function(a: A, b?: B) {
    if (typeof b !== 'undefined') return f(a, b);
    else
      return function(b: B): C {
        return f(a, b);
      };
  };
}

function filter<A, B extends A>(pred: (x: A) => x is B, l: A[]): B[];
function filter<A>(pred: (x: A) => boolean, l: A[]): A[] {
  return l.filter(pred);
}

const l1 = [1, 2, 3, 4, 5, '3', '4'];

const l2 = filter(x => typeof x === 'number', l1);

console.log(l2);
