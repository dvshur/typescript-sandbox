namespace MappedDemo2 {
  type O = {
    a: number;
    b: string;
  };

  // keyof operator
  type KeysO = keyof O;

  // in operator
  type B = { [K in KeysO]: string };
  type C = O['a'];

  // join them
  type SameAs<O2> = { [K in keyof O2]: O2[K] };
  type SameAsO = SameAs<O>;
  type Partial2<O, T extends keyof O = keyof O> = { [K in T]?: O[K] };
  type PartialO = Partial2<O>;
  type Nullable<O> = { [K in keyof O]: O[K] | null };
  type NullableO = Nullable<O>;

  // apply object to keys union to get values union
  type OValues = O[keyof O];

  // `never` special case
  type ON = { q: string; w: never };
  type OValues = ON[keyof ON];

  // extract key names to values
  type OKeysMap = { [K in keyof O]: K };

  // get key names union — other way
  // (apply previous step to keys union)

  // conditional mapped
  // 1. make fields that do not pass criteria `never`
  // 2. for other fields — take their name
  type OnlyNumberKeys<O> = {
    [K in keyof O]: O[K] extends number ? K : never
  }[keyof O];
  type ONumberKeys = OnlyNumberKeys<O>;
  // type ONumbersOnly = { [K in ONumberKeys]: O[K] }
  type ONumbersOnlyOneLine = Pick2<
    O,
    { [K in keyof O]: O[K] extends number ? K : never }[keyof O]
  >;

  // types
  // Record, Pick
  // type Record2<Keys extends string, ValueType> = { [K in Keys]: ValueType }
  // type Pick2<O, K extends keyof O> = { [K2 in K]: O[K2] }

  type C2 = Record<string, number>;

  const c: C2 = { q: 2, w: 2 };

  type WithoutId<O> = Pick<
    O,
    { [K in keyof O]: K extends 'id' ? never : K }[keyof O]
  >;

  type Tx = {
    id: string;
    type: 2;
  };

  type TxNoId = WithoutId<Tx>;

  type Omit<O, K extends keyof O> = Pick<
    O,
    { [K2 in keyof O]: K2 extends K ? never : K2 }[keyof O]
  >;

  type Difference<O, O2> = Pick<
    O,
    { [K2 in keyof O]: K2 extends keyof O2 ? never : K2 }[keyof O]
  >;

  type Tx2 = {
    type: 2;
    some: 3;
  };

  type T3 = Difference<Tx2, Tx> & Difference<Tx, Tx2>;

  const c2: T3 = { id: 'qwe', some: 3 };
}
