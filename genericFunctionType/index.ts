type Identity<T> = {
  (t: T): T;
};

// @todo is it possible?
// identity should have type Identity, preserving generic
const identity = x => x;

// expect n to be number
const n = identity(2);
