export interface Foldable<A> {
  reduce: <B>(cb: (acc: B, x: A) => B, a: A) => B;
}

class Monoid<T> {
  public readonly op: (t1: T, t2: T) => T;
  public readonly zero: T;

  constructor({ op, zero }: { op: (t1: T, t2: T) => T; zero: T }) {
    this.op = op;
    this.zero = zero;
  }

  public static concatAll = <T>(xs: Foldable<T>, m: Monoid<T>): T =>
    xs.reduce(m.op, m.zero);
}

export default Monoid;
