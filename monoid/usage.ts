import Monoid, { Foldable } from '.';
import * as Maybe from 'folktale/maybe';

// number monoids
const l: number[] = [1, 10, 20, 9];
const numberSumMonoid = new Monoid<number>({
  op: (x, y) => x + y,
  zero: 0,
});

const numberMultiplyMonoid = new Monoid<number>({
  op: (x, y) => x * y,
  zero: 1,
});

const numberMaxMonoidMonoid = new Monoid<number>({
  op: (a, b) => Math.max(a, b),
  zero: -Infinity,
});

// boolean
const booleanAndMonoid = new Monoid<boolean>({
  op: (a, b) => a && b,
  zero: true,
});
const booleanOrMonoid = new Monoid<boolean>({
  op: (a, b) => a || b,
  zero: false,
});

const lMaybes = [
  Maybe.Nothing(),
  Maybe.Just(1),
  Maybe.Nothing(),
  Maybe.Just(2),
];

const maybeMonoid = new Monoid<Maybe<number>>({
  op: (a, b) => a.orElse(() => b),
  zero: Maybe.Nothing(),
});

// console.log(lMaybes.reduce(maybeMonoid.op, maybeMonoid.zero));

// monoids compose ! yay

interface CustomType {
  n: number;
  b: boolean;
  maybeA: Maybe<number>;
}

const l3: CustomType[] = [
  {
    n: 10,
    b: false,
    maybeA: Maybe.Nothing(),
  },
  {
    n: 20,
    b: true,
    maybeA: Maybe.Just(2),
  },
];

const compositeMonoid = new Monoid<CustomType>({
  op: (a, b) => ({
    n: numberSumMonoid.op(a.n, b.n),
    b: booleanAndMonoid.op(a.b, b.b),
    maybeA: maybeMonoid.op(a.maybeA, b.maybeA),
  }),
  zero: {
    n: numberSumMonoid.zero,
    b: booleanAndMonoid.zero,
    maybeA: maybeMonoid.zero,
  },
});

// console.log(l3.reduce(compositeMonoid.op, compositeMonoid.zero));

// endoMonoid
type EndoFunction<A> = (a: A) => A;

const endoMonoid = <A>() =>
  new Monoid<EndoFunction<A>>({
    op: (f, g) => a => f(g(a)),
    zero: a => a,
  });

const fs = [
  (s: string) => s.toUpperCase(),
  (s: string) => s + '!',
  (s: string) => 'Dmitry' + s,
];

const f = Monoid.concatAll<EndoFunction<string>>(fs, endoMonoid());

console.log(f(', hello'));
