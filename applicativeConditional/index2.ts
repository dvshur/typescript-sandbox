// function (b: B) => C

const f2 = (n: number): string => '2';
const f3 = (n: number): boolean => true;

// type ReturnType2<F> = F extends (...args: any[]) => infer R ? R : never;

// declare const c2: ReturnType2<typeof f2>;
// declare const c3: ReturnType2<typeof f3>;
// declare const c4: ReturnType<4>;

// type one = 'a' | 'b' | 'c' | 'd'
// type two = 'b' | 'd'

// type three = Exclude<one, two>

// interface ApplicativeFunctor<A> {
//   value: A;
//   ap<B>(f: ApplicativeFunctor<B>): A extends (b: B) => any ? ApplicativeFunctor<ReturnType<A>> : never;
// }

// type FirstArg<Fn> = Fn extends (a: infer A, ...rest: any[]) => any ? A : never;

// const f4 = (b: number, c: string) => c;

// type C = FirstArg<typeof f4>

// type Either2<A, B> = {}