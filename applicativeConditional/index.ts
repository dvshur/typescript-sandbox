class Applicative<A> {
  constructor(public value: A) {}

  public apply<B>(
    f: Applicative<B>
  ): A extends (b: B) => any ? Applicative<ReturnType<A>> : unknown {
    const isFunction = (maybeFn: unknown): maybeFn is (b: B) => any =>
      typeof maybeFn === 'function';

    if (isFunction(this.value))
      return new Applicative(this.value(f.value)) as A extends (b: B) => any
        ? Applicative<ReturnType<A>>
        : unknown;
    else throw new Error('Applicative must contain function');
  }
}

const apWithFn = new Applicative((s: number) => s.toString() + '000');
const apWithFn2 = new Applicative((s: string) => parseInt(s));
const apWithValue1 = new Applicative(200);
const apWithValue2 = new Applicative('2000');

const b1 = apWithFn.apply(apWithValue1);
const b2 = apWithFn.apply(apWithValue2);
const b3 = apWithValue1.apply(apWithValue2);
const b4 = apWithFn2.apply(apWithValue2);
const b5 = apWithFn2.apply(apWithValue1);

const unsafeGet = <A>(b: Applicative<A>): A => b.value;
const c = unsafeGet(b2);
