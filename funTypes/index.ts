// logic in types
type X = { qewrqwer: 2 };

type FunType<F> = {
  isQ1: { q: 1 };
  isQ2: { q: 2 };
}[F extends X ? 'isQ1' : 'isQ2'];

type Application = FunType<number>
