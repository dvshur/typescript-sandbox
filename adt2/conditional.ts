const invert = <T extends string | number>(
  t: T
): T extends string ? number : string => {
  if (typeof t === 'string')
    return parseInt(t) as T extends string ? number : string;
  else return t.toString() as T extends string ? number : string;
};

// const invert = (t: string | number): string | number => {
//   if (typeof t === 'string') return parseInt(t);
//   else return t.toString();
// };

const a = 2;
const b = '2';

const c = invert(a);
const d = invert(b);
