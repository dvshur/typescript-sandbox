export type Some<A> = {
  readonly tag: 'Some';
  value: A;
};
export type None = {
  readonly tag: 'None';
};

export type Option<A> = Some<A> | None;

const of = <A>(a: A): Some<A> => ({
  tag: 'Some',
  value: a,
});

const none = (): None => ({
  tag: 'None',
});

// map, filter, flatMap, ...

export { of, none };
