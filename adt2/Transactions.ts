// enum TxType {
//   Genesis = 1,
//   Issue = 3,
// }

type Genesis = {
  tag: 'Genesis';
  type: 1;
};

type Issue = {
  tag: 'Issue';
  type: 3;
  version: 1;
};

type IssueV2 = {
  tag: 'IssueV2';
  type: 3;
  version: 2;
  script: string;
};

type Transaction = Genesis | Issue | IssueV2;

const f1 = (t: Transaction): Transaction => {
  switch (t.tag) {
    case 'Genesis':
      return t;
    case 'Issue':
      return t;
    case 'IssueV2':
      return t;
  }
};
