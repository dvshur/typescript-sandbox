namespace FalseOption {
  type Some<A> = {
    readonly tag: 'Some';
    value: A;
  };
  type None = {
    readonly tag: 'None';
  };

  type Option<A> = Some<A> | None;

  type Cat = { cat: true };

  const f = (a: Option<Cat>) => {
    switch (a.tag) {
      case 'Some':
        return a.value;
        break;
      default:
        a.value;
        break;
    }
  };
}
